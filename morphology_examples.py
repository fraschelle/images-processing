#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some examples of spectral decomposition and manipulation
"""

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

import filters as filt

path = './IMG/smiley_nb.png'
img = np.array(Image.open(path))
plt.imshow(img, cmap='gray')

def morphology(img,filt,method='sum'):
    """
    Multiply the element of the img array by the filt array and apply the method to the resulting  array.
    Usefull to make intermediary size filters of picture.
    It works for any dimension of array, providing img and filt have the same shape.
    """
    sf = filt.shape
    si = img.shape
    assert len(sf)==len(si), "Filter and Image must have the same shape"
    for s in sf: assert s%2==1, "Filter must be of odd size"
    # padding of the img array
    size = [si[i]+2*sf[i]//2+1 for i in range(len(si))]
    img0 = np.zeros(size,dtype=img.dtype)
    sl = [slice(sf[i]//2+1,-sf[i]//2) for i in range(len(si))]
    img0[tuple(sl)] = img
    # creates all the coordinates of the filt array
    coords = [(i,) for i in range(sf[0])]
    for r in range(1,len(si)):
        coords = [(c,*o) for c in range(sf[r]) for o in coords]
    # shifts the image elements and multiply by the element of the filter
    all_imgs = [np.roll(img0,c,range(len(c))) for c in coords if filt[c]==1]
    # apply the transformation over the different multiplied arrays
    res = getattr(np,method)(np.array(all_imgs), axis=0)
    # reshift the elements as i->i+1, j->j+1, k->k+1 since the multiplication 
    # applies to the previous element along the diagonal
    sl = [slice(sf[i]//2+2,-sf[i]//2+1) for i in range(len(si))]
    return res[tuple(sl)]

def create_es(size=3):
    assert size%2==1, "Size must be odd"
    es = np.ones([size,size])
    pos = [0,size-1]
    pos = [(i,j) for i in pos for j in pos]
    for p in pos: es[p] = 0
    return es

def erosion(img, size=3):
    assert img.ndim==2, "Image must be 2D array"
    es = create_es(size)
    return morphology(img,es,method='min')

def dilatation(img, size=3):
    assert img.ndim==2, "Image must be 2D array"
    es = create_es(size)
    return morphology(img,es,method='max')

ero = erosion(img, size=5)
plt.imshow(ero, cmap='gray')

dilat = dilatation(img, size=5)
plt.imshow(dilat, cmap='gray')
