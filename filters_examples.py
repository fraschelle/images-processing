#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some examples of filtering
"""

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import filters as filt

# path to image
path = "./IMG/lena_original.png"

# load an image using PIL, and convert it to numpy array
im = Image.open(path)
img = np.array(im)
plt.imshow(img, cmap='gray')

# one can import images using matplotlib as well
img0 = plt.imread(path)
# in that case the pixels are in [0,1] scale
mask = np.ceil(img0*255) == img
np.sum(mask) == np.prod(np.array(img.shape))

n, bins, patches = plt.hist(img.flatten(), bins=range(256))
plt.show()

stats = filt.display_statistics(img.flatten(), bins=range(256))

# display the picture on the console
plt.imshow(img, cmap='gray')

# reexposition
reex  = filt.reexpose(img)
plt.imshow(reex, cmap='gray')
sreex = filt.display_statistics(reex.flatten(), bins=range(256))

reco = filt.recontrast(img)
plt.imshow(reco, cmap='gray')
scontr = filt.display_statistics(reco.flatten(), bins=range(256))

# some filters

# mean filter
mean = filt.filter_mean(img, size=9)
plt.imshow(mean, cmap='gray')

# gaussian filter
gauss = filt.filter_gaussian(img, size=33)
plt.imshow(gauss, cmap='gray')

# median filter
med = filt.filter_median(img, size=5)
plt.imshow(med, cmap='gray')

# difference filter
diff = filt.filter_difference(img, size=3, weight=-2)
plt.imshow(diff, cmap='gray')

# min-max filter
minmax = filt.filter_minmax(img, size=3)
plt.imshow(minmax, cmap='gray')


