#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
All tools for filtering images
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

def display_statistics(x, bins=None, display=True):
    """
    Calculate the estimated histograms (using numpy.histogram function), and returns the distribution of a 1D numpy array
    
    Parameters
    ----------
    x : numpy array or list
        DESCRIPTION.
    bins : integer, optional
        number of bins in the estimators, or a range. 
        The default is 10.
    display : boolean
        Display or not the estimators. The default is True.

    Returns
    -------
    A dictionnary with entries 'histogram', 'frequencies', 'cumldist' and 'bins'.
    All are 1D numpy arrays of equal length.

    """
    assert x.ndim==1, "Requires 1D numpy array"
    res = {}
    if not bins:
        bins = 10
    histo, bins = np.histogram(x, bins=bins)
    res['histogram'] = histo
    bins = (bins[:-1]+bins[1:])/2
    res['bins'] = bins
    freqs = histo/np.sum(histo)
    res['frequencies'] = freqs
    cdf = np.cumsum(freqs)
    res['cumuldist'] = cdf
    if display is True:
        plt.hist(x, bins=bins)
        plt.ylabel('Frequencies')
        plt.xlabel('bins')
        plt.title('Histogram, in frequency')
        plt.show()
        plt.clf()
        plt.plot(bins, cdf)
        plt.ylabel('Density of probability')
        plt.xlabel('bins')
        plt.title('Cumulative distribution function')
        plt.show()
        plt.clf()
    return res

def reexpose(img, levels=256):
    """
    Change the exposition of the picture.
    The initial image should be a black and white one.
    """
    imin = np.min(img)
    imax = np.max(img)
    res = (img-imin)/(imax-imin)
    res = np.floor((levels-1)*res)
    return res

def recontrast(img, levels=256):
    """
    Change the contrast of a picture, according to the method explained in
    https://en.wikipedia.org/wiki/Histogram_equalization

    Parameters
    ----------
    img : a numpy array
        DESCRIPTION.
    levels : an integer, optional
        The number of levels/intensity in the array. The default is 256.

    Returns
    -------
    A numpy array of same shape as the input.

    """
    assert img.ndim in [2,3,], "Requires a 2D or 3D numpy array"
    if img.ndim == 3:
        i0 = [img[:,:,i] for i in range(img.shape[2])]
    elif img.ndim == 2:
        i0 = [img,]
    histo, bins = np.histogram(img.flatten(), bins=range(levels))
    c = np.cumsum(histo)
    cmin = c[c>0][0]
    c -= cmin
    dim = np.prod(np.array(img.shape))
    c = np.ceil(np.divide(c,dim-cmin)*(levels-1))
    c = np.asarray(c, dtype=int)
    res = np.zeros(img.shape, dtype=int)
    for i in range(len(bins)-1):
        mask = (bins[i]<img) * (img<=bins[i+1])
        res[mask] = c[i]
    return res
    
def compare_img(img1,img2):
    """
    Give a distance between two images.
    """
    assert img1.shape==img2.shape, "Images must have the same size"
    res = 100*(img1-img2)/img2
    mean = np.mean(res.reshape(-1))
    std = np.std(res.reshape(-1))
    print("Distance: %.2f +/- %.2f" %(abs(mean),std))
    return res

def convolve(img,filt,method='sum'):
    """
    Multiply the element of the img array by the filt array and apply the method to the resulting  array.
    Usefull to make intermediary size filters of picture.
    It works for any dimension of array, providing img and filt have the same shape.
    """
    sf = filt.shape
    si = img.shape
    assert len(sf)==len(si), "Filter and Image must have the same shape"
    for s in sf: assert s%2==1, "Filter must be of odd size"
    # padding of the img array
    size = [si[i]+2*sf[i]//2+1 for i in range(len(si))]
    img0 = np.zeros(size,dtype=img.dtype)
    sl = [slice(sf[i]//2+1,-sf[i]//2) for i in range(len(si))]
    img0[tuple(sl)] = img
    # creates all the coordinates of the filt array
    coords = [(i,) for i in range(sf[0])]
    for r in range(1,len(si)):
        coords = [(c,*o) for c in range(sf[r]) for o in coords]
    # shifts the image elements and multiply by the element of the filter
    all_imgs = [np.roll(img0,c,range(len(c)))*filt[c] for c in coords]
    # apply the transformation over the different multiplied arrays
    res = getattr(np,method)(np.array(all_imgs), axis=0)
    # reshift the elements as i->i+1, j->j+1, k->k+1 since the multiplication 
    # applies to the previous element along the diagonal
    sl = [slice(sf[i]//2+2,-sf[i]//2+1) for i in range(len(si))]
    return res[tuple(sl)]

def filter_mean(img,size=3):
    """
    Calculate the mean filter of size 'size' on the image
    """
    assert img.ndim==2, "Image must be 2D arrays"
    filt = np.full([size,size],1)
    filt = filt/np.sum(filt)
    return convolve(img,filt,method='sum')

def filter_gaussian(img,size=3):
    """
    Calculate the Gaussian filter (i.e. Gaussian blur) on the image
    """
    assert img.ndim==2, "Image must be 2D arrays"
    filt = signal.gaussian(size,1)
    filt = filt*filt.reshape(-1,1)
    filt = filt/np.sum(filt)
    return convolve(img,filt,method='sum')

def filter_median(img,size=3):
    """
    Apply a median filter to the image
    """
    assert img.ndim==2, "Image must be 2D arrays"
    filt = np.full([size,size],1)
    filt = filt/np.sum(filt)
    return convolve(img,filt,method='median')

def filter_difference(img, size=3, weight=1):
    """
    Apply a difference filter to the image
    """
    assert img.ndim==2, "Image must be 2D arrays"
    assert size%2==1, "Filter size must be odd"
    filt = np.full([size,size],-weight)
    filt[size//2,size//2] = weight*size*size
    return convolve(img,filt,method='sum')

def filter_minmax(img, size=3):
    """
    Apply a min/max filter to the image
    """
    assert img.ndim==2, "Image must be 2D arrays"
    filt = np.full([size,size],1)
    fmax = convolve(img,filt,method='max')
    fmin = convolve(img,filt,method='min')
    favg = (fmax+fmin)/2
    return np.where(img >= favg, fmax, fmin)    

def spectrum(img, display=True):
    """
    Calculate the amplitude and phase spectrum of an image.
    Returns a dictionnary with amplitude, phase, and spectrum of the initial image.
    """
    spec = np.fft.fftn(img,s=img.shape)
    ampl = np.abs(spec)
    phas = np.arctan2(np.imag(spec),np.real(spec))
    dic = dict(
        spectrum=spec,
        amplitude=ampl,
        phase=phas,
        )
    if display == True:
        freqs = [np.fft.fftfreq(d) for d in spec.shape]
        plt.imshow(np.log(ampl), cmap='gray')
        plt.title("Amplitude, in logarithm scale")
        plt.show()
        plt.clf()
        plt.imshow(phas, cmap='gray')
        plt.title("Phase")
        plt.show()
        plt.clf()
    return dic

def filter_spectral_rectangular(img, p=5, display=True, method='flat'):
    """
    Separate the amplitude of an image in low/high frequencies.
    Plot the different images if display==True.
    Returns a dictionnary with
     - low_pass_filter
     - high_pass_filter
    arrays.
    """
    spect = spectrum(img, display=False)
    amp = spect['amplitude']
    phase = spect['phase']
    filt = []
    for f in [amp,phase]:
        freqs = [2*np.fft.fftfreq(d) for d in f.shape]
        masks = [np.abs(f)>p/100 for f in freqs]
        indices = [list(np.arange(len(m))[m]) for m in masks]
        slices = [slice(np.min(i),np.max(i)+1) for i in indices]
        temp = np.zeros(f.shape)
        temp[tuple(slices)] = f[tuple(slices)]
        filt.append(temp)
        temp = np.copy(f)
        temp[tuple(slices)] = 0
        filt.append(temp)
    img_lpf = np.fft.ifftn(filt[0]*np.exp(1j*filt[2])).real
    img_hpf = np.fft.ifftn(filt[1]*np.exp(1j*filt[3])).real
    if display == True:
        plt.imshow(img, cmap='gray')
        plt.title("Original image")
        plt.show()
        plt.clf()
        plt.imshow(img_hpf, cmap='gray')
        plt.title("High-pass filter image")
        plt.show()
        plt.clf()
        plt.imshow(img_lpf, cmap='gray')
        plt.title("Low-pass filter image")
        plt.show()
        plt.clf()
    dic = dict(
        low_pass_filter=img_lpf,
        high_pass_filter=img_hpf,
        )
    return dic

